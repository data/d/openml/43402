# OpenML dataset: Stock-Market-NIFTY50-Index-Data

https://www.openml.org/d/43402

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Stock Market forecasting and Modelling has always been the problem most researched by analysts, here we will present a dataset of the Indian Stock Exchange - Nifty50 Index . for use for modelling and forecasting ability by machine learning .
Content
The Data consists of 9 Rows, directly sources from the NSE India website and uploaded here each data row represents the market prices at the Close of day . along with the NIFTY50 Index Daily yields.  the dataset is from 2006 to 2020 Feb. ideal for making models of daily datasets.
    
 Data Field  Description 
 Date  --Calendar Date: Trading Date 
 Open  --Index Open Price for Day 
 High  --Index Highest Price for Day 
 Low  ---Index Lowest Price for Day 
 Close  --Index Closing Price for Day 
 Volume  --Total Volume Traded During the Day  
 Pe ratio  --The Profit/Equity ratio of NIFTY Index 
 Pb ratio --The Profit/Book Value Ratio of Nifty Index 
 dividend_yield --The dividend yield of the Nifty Index 
    
Acknowledgements
The data is publicly sourced from the NSE India Website, and can be directly downloaded from 
www.nseindia.com
Inspiration
The Next greatest prediction algorithm to forecast market movement.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43402) of an [OpenML dataset](https://www.openml.org/d/43402). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43402/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43402/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43402/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

